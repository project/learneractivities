# LearnerActivities

LearnerActivities intends to pull the LinkedIn Learner Activities from LinkedIn 
Learning and store in Drupal in readable format.

- For a full description of the module, visit the 
  [project page](https://www.drupal.org/project/learneractivities)

- To submit bug reports and feature suggestions, or to track changes
  [issue queue](https://www.drupal.org/project/issues/search/learneractivities)


## Contents of this file

- Features
- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers

   
## Features

- Pull all the learning activities
- Cron based option
- List view avaiable for records "/linkedin-learner-activities"
- Rest Services is avaiable for use (/linkedin_learner_activities_rest)
    - Filter By  : Name (linkedin_learner_activities_rest?_format=json&name=ang)
    - Filter By : Email 
	  (linkedin_learner_activities_rest?_format=json&email=ang)
    - Filter By : Status 
	  (linkedin_learner_activities_rest?_format=json&status=In Progress)
    - Filter By : External_id 
	  (linkedin_learner_activities_rest?_format=json&external_id=200)
	
- EASY to modify the configration here 
  (learneractivities\src\Services\GetLearnerActivities.php), get the detailed 
  [configuration](https://learn.microsoft.com/en-us/linkedin/learning/reference/learning-activity-reports-reference)
 
 
- By Default module will store data either completed or In Progress and keep 
  the last view date in changed coulumn
    - If you want to keep all tracks feel free to visit below file and 
	  read the comment Line No : 44
       - learneractivities\src\Plugin\QueueWorker\LilLearnerActivitiesQueue.php
 

## Requirements

No special requirements.


## Recommended modules

- views
- views_ui 
- [queue_ui](https://www.drupal.org/project/queue_ui) 
- [ultimate_cron](https://www.drupal.org/project/ultimate_cron)

 
## Installation

Install as you would normally install a contributed Drupal module. See
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules) for further information.


## Configuration

- Configure the LearnerActivities at `/admin/config/linkedin_configuration`.


## Maintainers

- [Md Hafizor Rahman](https://www.drupal.org/u/md-hafizor-rahman)
