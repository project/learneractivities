<?php


namespace Drupal\learneractivities\Plugin\QueueWorker;


use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Save the queue item in a custom entitities (learneractivities)
 *
 * To process the queue items whenever Cron is run,
 * we need a QueueWorker plugin with an annotation witch defines
 * to witch queue it applied.
 *
 * @QueueWorker(
 *   id = "lillearnerActivities_queue",
 *   title = @Translation("Import Learner Activities from LinedIn Learning"),
 *   cron = {"time" = 60}
 * )
 */
 
 
 
 class LilLearnerActivitiesQueue extends QueueWorkerBase {
	
    /**
	* {@inheritdoc}
	*/
	public function processItem($item) {
		// Save the queue item in a node
		
		
		//check the combination of external id along with course_id
		//if exist update status, changed
		//else create new entry
		
		$query = \Drupal::entityQuery('learneractivities');
		$query->condition('external_id', $item->external_id);
		$query->condition('course_id', $item->course_id);
		$entity_id = $query->execute();
		//\Drupal::logger('learneractivities')->error('Second Service' . json_encode($entity_id)); 

		// If you want to keep all tracks comment whole if conditions and move 60 -66 out of else
		if($entity_id){
			//get the entity id
			$entity_id = array_keys($entity_id)[0];
			
			//load the entity by entity_id
			$cnode = \Drupal::entityTypeManager()->getStorage('learneractivities')->load($entity_id);
			

			//update
			$cnode->set('changed', $item->changed);
            $cnode->set('status', $item->status);
			$cnode->set('percent', $item->percent);
            $cnode->save();
		}
		else {
			//create
			$cnode = \Drupal::entityTypeManager()->getStorage('learneractivities')->create(
				['type' => 'learneractivities', 'course_title' => $item->course_title, 'external_id' => $item->external_id,
				 'email' => $item->email, 'status' => $item->status , 'created' => $item->created,
				 'changed' => $item->changed, 'groups' => $item->groups, 'course_id' => $item->course_id, 'name' => $item->name, 'percent' => $item->percent, 'course_language' => $item->course_language]);

			$cnode->save();
		}
		
		

	  
	}	
	 
 }