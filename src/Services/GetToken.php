<?php
namespace  Drupal\learneractivities\Services;

/**
* @Generate Bearer Token
*
*/

class GetToken {
    

	
	 public function GetToken(){
		 
		// Get all configuration to generate token
	
		$config = \Drupal::config('learneractivities.admin_settings');
		
		//read the configuration
		// Simple validation : URL should be there
		if(empty($config->get('oauth_url')) || empty($config->get('client_id')) || empty($config->get('client_secret')) || empty($config->get('grant_type')) ){
			\Drupal::logger('learneractivities')->notice('Token Generation Failed, Please save configuration first');
			return "failed";
		}
		
		$oauth_url = $config->get('oauth_url');
		$client_id = $config->get('client_id');
		$client_secret = $config->get('client_secret');
		$grant_type = $config->get('grant_type');
		 
		  $params = "grant_type=$grant_type&client_id=$client_id&client_secret=$client_secret";
		//Open connection
		  $curl_handle = curl_init();
		  //Set the url, POST data
		  curl_setopt($curl_handle, CURLOPT_URL, $oauth_url);
		  curl_setopt($curl_handle, CURLOPT_POST, 1);
		  curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $params);
		  curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
		  curl_setopt($curl_handle, CURLOPT_AUTOREFERER, TRUE);
		  curl_setopt($curl_handle, CURLOPT_FOLLOWLOCATION, TRUE);
		  curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, FALSE);
		  $result = curl_exec($curl_handle);
			
		if (!curl_errno($curl_handle)) {
				$arr_token_details = json_decode($result);
				$access_token = $arr_token_details->access_token;
				return $access_token;
			}
			else {
				 return "failed";
			}
		  //Close connection
		  curl_close($curl_handle); 
	 }
 
}