<?php
namespace  Drupal\learneractivities\Services;


use Drupal\Core\Queue\QueueFactory;


/**
* @Pull Learning actvities from LinkedIn
*
*/

class GetLearnerActivities { 

  
  /**
   * Symfony\Component\DependencyInjection\ContainerAwareInterface definition.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerAwareInterface
   */
  protected $queueFactory;
  
  
  /**
   * Inject services.
   */
  public function __construct(QueueFactory $queue) {
    $this->queueFactory = $queue;


  }

  public function GetLearnerActivities($bearer_token){
    
	\Drupal::logger('learneractivities')->info('Second Service' . $bearer_token); 
	

	/*********************************************************************************
	**Invoke direct service direclty to create queue instead of going through dependcy injection																	****
	//$queue = \Drupal::service('queue')->get('lillearnerActivities_queue');
	**																	**************	
	**********************************************************************************/
	
	//get the configuration
	$config = \Drupal::config('learneractivities.admin_settings');
	$learningactivityreports_url = $config->get('learningactivityreports_url');
	
	//prepare the URL
	

	
	//Create queue to reduce load on the CPU 
	
	$currenttime = time();
	//Consider only last to last day due to delay in report availability on LinkedIn side.
	$currenttime = strtotime('-2 day', $currenttime)* 1000;
	
	$q_params = 'aggregationCriteria.primary=INDIVIDUAL&aggregationCriteria.secondary=CONTENT&q=criteria&contentSource=ALL_SOURCES&start=0&count=1000&assetType=COURSE&timeOffset.duration=1&timeOffset.unit=DAY&startedAt='.$currenttime;
	
	$token = $bearer_token; 

	$lil_url = $learningactivityreports_url ."?".$q_params;	


	$curl_handle = curl_init();
	curl_setopt($curl_handle, CURLOPT_URL, $lil_url);
	curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-type: application/json', "Authorization: Bearer " . $token));
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl_handle, CURLOPT_AUTOREFERER, TRUE);
	curl_setopt($curl_handle, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, FALSE);
	$result = curl_exec($curl_handle);
	
	if (!curl_errno($curl_handle)) {
		$result_data = json_decode($result);
		if ($result_data != FALSE) {
			//Result rows exists.
			if(isset($result_data->paging->total) && intval($result_data->paging->total) > 0) {
				 
				 foreach ($result_data->elements as $obj_ele) {
					$item = new \stdClass(); 
					if (isset($obj_ele->learnerDetails->email)) {
						$item->email = $obj_ele->learnerDetails->email;
					}
					if (isset($obj_ele->learnerDetails->name)) {
						$item->name = $obj_ele->learnerDetails->name;
					}
					else {
						$item->name = $obj_ele->learnerDetails->email;
					}
					if (isset($obj_ele->learnerDetails->uniqueUserId)) {
						$item->external_id = $obj_ele->learnerDetails->uniqueUserId;
					}
					else {
						$item->external_id = $obj_ele->learnerDetails->email ;
					}
					if (isset($obj_ele->learnerDetails->enterpriseGroups) && !empty($obj_ele->learnerDetails->enterpriseGroups)) {
						$item->groups = substr(implode(',', $obj_ele->learnerDetails->enterpriseGroups), 0,1000);
					}
					else {
						$item->groups = 'No Group Assigned';
					}
					if (isset($obj_ele->contentDetails->name)) {
						$item->course_title = substr($obj_ele->contentDetails->name, 0, 1000);
					}
					if (isset($obj_ele->contentDetails->locale->language)) {
						$item->course_language = $obj_ele->contentDetails->locale->language;						
					}
					else {
						$item->course_language = 'Language not found';
					}
					if (isset($obj_ele->contentDetails->contentUrn)) {
						$item->course_id = str_replace('urn:li:lyndaCourse:', '', $obj_ele->contentDetails->contentUrn);						
					}
					
					$item->status = 'In Progress';
					$item->changed =   time();
					$item->created =   time();
					$item->percent = 0;
					if (!empty($obj_ele->activities)) {						
						foreach ($obj_ele->activities as $obj_act) {
							
							if ($obj_act->engagementType == 'PROGRESS_PERCENTAGE') {
								$item->percent = $obj_act->engagementValue;
								$item->created =   round($obj_act->firstEngagedAt/1000);
								$item->changed =  round($obj_act->lastEngagedAt/1000);
							}
							if ($obj_act->engagementType == 'COMPLETIONS') {
								$item->status = 'Completed';
							}
						}	
					}
					$queue = $this->queueFactory->get('lillearnerActivities_queue');
					$queue->createItem($item);
					
				}
					
				
					
			}
		}

	}
	
	else {
		\Drupal::logger('learneractivities')->error('Failed to pulled the data from LinkedIn Learning'); 
	}
	curl_close($curl_handle); 
	
	
	
	
     

	 
  }
  
  
  
}