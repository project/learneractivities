<?php

namespace Drupal\learneractivities\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Use this form to set the variable
 */
class LinkedInConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'learneractivities_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'learneractivities.admin_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('learneractivities.admin_settings');
	
    $form['oauth_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('OAUTH URL'),
      '#description' => $this->t('Enter OAUTH URL of LinkedIn for token generation.'),
      '#default_value' => $config->get('oauth_url'),
    ];

    $form['client_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client Id'),
      '#description' => $this->t('Enter Client Id.'),
      '#default_value' => $config->get('client_id'),
    ];

    $form['client_secret'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client Secret'),
      '#description' => $this->t('Enter Client Secret.'),
      '#default_value' => $config->get('client_secret'),
    ];

    $form['grant_type'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Grant Type'),
      '#description' => $this->t('Enter Grant Type.'),
      '#default_value' => $config->get('grant_type'),
    ];


    $form['learningactivityreports_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Learning Activity Reports url'),
      '#description' => $this->t('Enter Learning Activity Reports url to pull report from LIL.'),
      '#default_value' => $config->get('learningactivityreports_url'),
    ];





    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

	$this->config('learneractivities.admin_settings')
      ->set('oauth_url', $form_state->getValue('oauth_url'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('grant_type', $form_state->getValue('grant_type'))
      ->set('learningactivityreports_url', $form_state->getValue('learningactivityreports_url'))
      ->set('query_param', $form_state->getValue('query_param'))
      ->save();
	
	parent::submitForm($form, $form_state);
    
  }

}
