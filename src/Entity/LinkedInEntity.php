<?php

namespace Drupal\learneractivities\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;



/**
 * Defines the Learner activites entity.
 * Below are the fields that needs to be created with custom entity  
    email
    name
    external_id
    groups
    course
    course_language
    course_id
    status
    changed
    percent
    started
 * @ingroup offer
 *
 * @ContentEntityType(
 *   id = "learneractivities",
 *   label = @Translation("Learner Activities"),
 *   base_table = "learneractivities",
 *   data_table = "learneractivities_field_data",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "course_title",
 *   },
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 * )
 */
 
 
 class LinkedInEntity extends ContentEntityBase {
	public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
		$fields = parent::baseFieldDefinitions($entity_type); 

		$fields['external_id'] = BaseFieldDefinition::create('string')
		  ->setLabel(t('External Id'))
		  ->setDescription(t('External id like SSO'))
		  ->setSettings([
			'max_length' => 255,
			'text_processing' => 0,
		  ]);

		$fields['course_title'] = BaseFieldDefinition::create('string')
		  ->setLabel(t('Title'))
		  ->setDescription(t('The title of the course'))
		  ->setSettings([
			'max_length' => 1000,
			'text_processing' => 0,
		  ]);
		  
		  
		$fields['email'] = BaseFieldDefinition::create('string')
		  ->setLabel(t('Email'))
		  ->setDescription(t('Learner Email ID'))
		  ->setSettings([
			'max_length' => 255,
			'text_processing' => 0,
		  ]); 

        $fields['name'] = BaseFieldDefinition::create('string')
		  ->setLabel(t('Name'))
		  ->setDescription(t('Name of the learner'))
		  ->setSettings([
			'max_length' => 255,
			'text_processing' => 0,
		  ]); 		  
		  
		  
		$fields['course_id'] = BaseFieldDefinition::create('string')
		  ->setLabel(t('Course Id'))
		  ->setDescription(t('Course Id'))
		  ->setSettings([
			'max_length' => 255,
			'text_processing' => 0,
		  ]);    
		  
		  
		  
		$fields['groups'] = BaseFieldDefinition::create('string')
		  ->setLabel(t('Groups'))
		  ->setDescription(t('Group the course belongs'))
		  ->setSettings([
			'max_length' => 1000,
			'text_processing' => 0,
		  ]);

        $fields['course_language'] = BaseFieldDefinition::create('string')
		  ->setLabel(t('Course Language'))
		  ->setDescription(t('Language of the course'))
		  ->setSettings([
			'max_length' => 255,
			'text_processing' => 0,
		  ]);	

        $fields['percent'] = BaseFieldDefinition::create('string')
		  ->setLabel(t('Percentage'))
		  ->setDescription(t('Percentage of completion'))
		  ->setSettings([
			'max_length' => 255,
			'text_processing' => 0,
		  ]);			  


		$fields['status'] = BaseFieldDefinition::create('string')
		  ->setLabel(t('Status'))
		  ->setDescription(t('Status of the course activites like In Progress, Completed'))
		  ->setSettings([
			'max_length' => 100,
			'text_processing' => 0,
		  ]);






		$fields['created'] = BaseFieldDefinition::create('created')
		  ->setLabel(t('Created'))
		  ->setDescription(t('The time that the entity was created.'));

		$fields['changed'] = BaseFieldDefinition::create('changed')
		  ->setLabel(t('Changed'))
		  ->setDescription(t('The time that the entity was last edited.'));

		return $fields;
	}
 }